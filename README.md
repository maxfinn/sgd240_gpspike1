﻿# Spike Report

## Material Editor

![Each of the instanced materials][Materials01]

### Introduction

The Unreal Engine material system is vast, powerful, and intimidating.

We want to be able to utilise the basics of the material system to help out our artists, and potentially even be able to empower them with new capabilities that they didn’t know we had!

### Goals

1. Tech: What capabilities does the Unreal Engine Materials system have?
1. Skill: How to create and utilise Unreal Engine Materials
1. Knowledge: What are the best practices when creating and organising materials in UE4 
1. Knowledge: What are Material Instances and why should we use them?
1. Knowledge: What is a PBR Material? 
1. Knowledge: What PBR workflow does Unreal Engine use, and how does it compare to other programs (and are there any terminology differences – and what are they)? 
1. Tech: Why do people recommend using a single texture parameter which include Roughness, Specular, and AO maps all in one?

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Physically Based Materials](https://docs.unrealengine.com/latest/INT/Engine/Rendering/Materials/PhysicallyBased/index.html)
* [Using GameTextures In Unreal Engine 4](https://www.gametextures.com/using-gametextures-in-unreal-engine-4/)
* [Creating and using Material Instances](https://docs.unrealengine.com/latest/INT/Engine/Rendering/Materials/HowTo/Instancing/index.html)
* [Using Texture Masks](https://docs.unrealengine.com/latest/INT/Engine/Rendering/Materials/HowTo/Masking/index.html)

### Tasks Undertaken

1. Create ten spheres in a new scene. As we create each material instance, we will apply them to a sphere so that we can view them in game.
1. Create a material called M_Basic_PBR and add a vector and three scalar values (hold 3 or 1 and click). Optionally name these parameters and give them a category.
    1. Set their defaults as (1.0, 1.0, 1.0), 0.0 (Metallic), 0.5 (Specular) and 0.25 (Roughness).
    1. Right click on the material in the content browser and create six material instances from it.
    1. Opening them one at a time, enable the parameters and adjust them as required to make the various sub-materials.
1. Create a material called M_Translucent in the same way as M_Basic_PBR, but also add one more scalar parameter, and set it to 1.0 (Opacity). Set this material's blend mode to Translucent.
    1. Create two material instances. Open each, and set opacity values for both (eg. 0.25 and 0.75).
1. Create a material called M_Masked, again in the same way, but replacing the scalar opacity with a texture sample parameter 2D.
    1. Import a texture via the content browser and use this as the texture parameter.
    1. Use a Math LERP to switch between two base colours (using white and grey as our defaults).
    1. Create one material instance, and change the base colours.
1. Create a material called M_Textured, but in this case, create four texture sample parameter 2D nodes.
    1. Link the colour socket of the first, third and fourth nodes to Base Color, Normal and Ambient Occlusion respectively. For the second node, link it's red channel to Roughness, green channel to Metallic, and blue channel to Specular.
    1. Import suitable textures just as we did for M_Masked and apply them to each texture parameter.
    1. In this case, we needn't adjust the material instance after creating it, but we could swap out textures if required.

### What we found out

Unreal Engine has a highly capable materials system which uses PBR to create lifelike materials. Most if not all real world materials can be recreated to a relatively high degree of accuracy, with the possibility of metallic and non-metallic materials, lacquered materials, transparent materials like glass and water, and even more complex materials like skin.

Materials can be created in a folder of their own, and optionally separated further based on usage. More general base materials can be created, and then extended upon as material instances.

Material instances allow us to take those general materials with a particular set of values, and adjust only those values so as to make different versions of the base material. Simplistic examples would include having a base material for cushion fabric and then using material instances to allow for different coloured cushions without as much cost recreating each colour in a separate material.

PBR stands for Physically Based Rendering, and is a digital rendering technique which mimics the way light and materials behave in the real world to produce far more realistic visuals. Particular areas of note would be light falloff and shadows.

Unreal Engine uses the Metalness PBR workflow. The main alternative is the Specular/Roughness workflow, which is utilised in Unity 5, amoung others. As noted in the `Using GameTextures in Unreal Engine 4` guide, "in [the] Metalness workflow a metallic material takes its specular color from the base material, and in [the] roughness/ specular workflow the metal takes its color from the specular texture" additionally, the Roughness value appears to be handled somewhat differently, requiring an inversion of the green channel, as well as the data being stored in linear colour space in the Metalness workflow.

Roughness, Specular, AO and Metalness maps all only require greyscale (or singular) values for a given point. A texture parameter can hold up to four of these in its red, blue, green and alpha channels, which translates to less overhead and smaller file sizes over a large number of materials. In general, it is just less wasteful to combine them together, and reduces texture clutter as well.

Comparing Masked and Translucent blend modes, it would appear that Translucent works as a material wide thing, and would be suitable for things such as clear glass and water, whereas Masked allows for differing opacity values over the material, making it suitable for things like stained glass windows and glass with condensation on it.

### Open Issues/Risks

1. The translucent materials seemingly do not cast a shadow.
    * This can perhaps be adjusted depending on the lighting model used, or something similar, but it may also have something to do with specific lights used in the scene. In any case, it is uncertain what the correct solution would be at this time.
1. The TLM Surface translucency lighting mode appears to have been removed or replaced in version 4.15 of UE4.

### Appendix

Figure 1: The setup for the Masked material.

![Masked material setup][Materials02]

Figure 2: With the Shader Complexity view, we can see that although all the materials are still of fairly low complexity, the two translucent materials are noticeably more complex than the others, and the fully textured material has a slight boost in complexity also.

![Shader complexity][Materials03]

[Materials01]: https://monosnap.com/file/OT4mh05V1pE9aD56HCCcX2WDI0P94N.png "Materials in a circle"
[Materials02]: https://monosnap.com/file/ZaZa9yY0YJGvZlDbkOHNftJNDMKEEE.png "Masked material setup"
[Materials03]: https://monosnap.com/file/6yts1i2P8ZkcGBiVo0v1ND6K3M8BYx.png "Shader complexity"